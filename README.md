Installation
=================

  * [Preliminaries](#preliminaries)
  * [Docker Toolbox](#docker-toolbox)
  

# Preliminaries

This demo uses Keras (with Tensorflow backend).
In order to facilitate and speed up the set up of the *environment* needed to run the demos, we will use Docker. 

# Docker Toolbox


The easiest way to get up an running is to use Docker. Docker is a way of managing a "virtual" Linux machine on your computer which will aid the creation a machine capable of running Tensorflow. First, please download and install the [Docker Toolbox](https://www.docker.com/products/docker-toolbox).

With this installed, you'll then need to run the "Docker Quickstart Terminal" which will launch a Terminal environment running on a virtual Linux machine on your computer. A virtual machine is basically an emulation of another machine. This is important because we'll use this machine to run Linux and install all of the necessary libraries for running Keras using the Tensorflow backend.

Once the Docker Quickstart Terminal is launched, run the following command:

```
$ docker-machine ip
```

You should see the virtual machine’s IP address as a result of the last command. This is the location of your virtual machine. NOTE THIS IP ADDRESS!

We can now run the docker image that contains everything we need to run Keras using Tensorflow backend, python, and jupyter notebook (where `[yourdir]` is the directory where you want to have you code):

```
$ cd [yourdir]
$ docker run -it -p 8888:8888 -v /$(pwd):/notebooks --name tfk mpimentel/tf-keras:devel
```

*Go for a coffee after typing the last command!*

Whenever you want to start this container again, you will launch the Docker QuickStart Terminal and then write: 

```
$ cd [yourdir]
$ docker start -i tfk
```

Notice that the command prompt will now be `#` instead of `$`. Now, try to go to the (empty) directory:

```
# cd /notebooks
```

And then git clonning this repo:

```
# git clone https://gitlab.com/mpimentel/tutorials-deep-learning.git
```

Finally, launch to jupyter notebook:

```
# jupyter notebook
```