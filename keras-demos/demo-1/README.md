In this demo, we will look at the International Airline Passengers prediction problem.

This is a problem where, given a year-month, the task is to predict the number of internation airline passengers in units of 1,000. See folder `data/` for a description of the dataset (*international-ailine-passengers.csv*).
Here's how the data look like in the file:

```
Month	International airline passengers: monthly totals in thousands. Jan 49 ? Dec 60
1949-01	112
1949-02	118
1949-03	132
1949-04	129
...
```

We phrase the time series prediction problem as a regression problem; i.e., given the number of passengers (in units of thousands) this month, what is the number of passengers next month.