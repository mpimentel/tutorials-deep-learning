# Data
This directory simply contains some of the (small) datasets used in the demos contained in this project. Although this is not the ideal place to keep data, I did not find an easy way to download the dataset directly from the web using a simple command.

## International Airline Passengers dataset
This is a dataset used, typically, in a problem where, given a year-month, the task is to predict the number of international airline passengers in units of 1,000. 

The data ranges from January 1949 to December 1960 (12 years), with 144 (=12*12) observations. The dataset is available for free from the [DataMarket webpage as a .csv download](https://datamarket.com/data/set/22u3/international-airline-passengers-monthly-totals-in-thousands-jan-49-dec-60#!ds=22u3&display=line).

**Filename:** *international-airline-passengers.csv*
